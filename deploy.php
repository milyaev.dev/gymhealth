<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'GymHealth');

// Project repository
set('repository', 'https://gitlab.com/milyaev.dev/gymhealth.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

host('production')
    ->hostname('77.222.62.66')
    ->user('milyaev')
    ->port(22)
    ->set('deploy_path', '/var/www/test')
    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no');
