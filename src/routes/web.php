<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Welcome;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('locale/{locale}', function ($locale){
//     Session::put('locale', $locale);
//     return redirect()->back();
// });


// Index

Route::get('/', 'EventController@index')->name('home');

// Auth Controller
/*
 * If
 * haven't Start Session
 * then
 * go to welcome
 * else /home
 */
Route::get('/welcome', 'Auth\LoginController@welcome')->name('welcome') ;

Route::get('/auth/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/auth/login', 'Auth\LoginController@login')->name('login');

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');
Route::get('/auth/sign-up', 'Auth\RegisterController@showRegisterForm')->name('signUp');
Route::post('/auth/sign-up', 'Auth\RegisterController@register')->name('signUp');



Route::get('/auth/forget-password', 'Auth\AuthController@forgetPassword')->name('forgetPassword');
Route::get('/auth/login/social-media', 'Auth\AuthController@loginSocialMedia')->name('loginSocialMedia');

// User Controller
Route::get('/profile', 'UserController@profile')->name('user.profile');
Route::get('/profile/edit', 'UserController@editProfile')->name('user.profile.edit');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Event Controller

Route::get('/event/workout', 'EventController@workout')->name('event.workout');
Route::get('/event/meal', 'EventController@meal')->name('event.meal');
Route::get('/event/sleep', 'EventController@sleep')->name('event.sleep');


//Admin Panel
Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'Admin',
        'middleware' => ['admin', 'can:admin-panel'],
    ],
    function () {
        Route::resource('users', 'UsersController');
        Route::get('/index', 'DashboardController.@index');
        Route::get('users/{user}/photo/delete', 'UsersController@deletephoto')->name('users.photo.delete');
        Route::get('users/{user}/verify', 'UsersController@verify')->name('users.verify');
        Route::get('users/{user}/set/trainer', 'UsersController@setTrainer')->name('users.set.trainer');
        Route::get('users/{user}/set/user', 'UsersController@setUser')->name('users.set.user');
        Route::get('users/{user}/set/banned', 'UsersController@setBanned')->name('users.set.banned');
        Route::get('users/{user}/set/wait', 'UsersController@setWait')->name('users.set.wait');
        Route::get('/home', 'HomeController@index')->name('home');
    }
);






