export default {
    "en": {
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds.",
            "make_auth": "Authorize yourself",
            "authorization": "Authorization",
            "login": "Login",
            "logout": "Logout",
            "login_field": "Login",
            "password_field": "Password",
            "back_to_the_main_page": "Back to the main page"
        },
        "pagination": {
            "previous": "« Previous",
            "next": "Next »"
        },
        "validations": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, dashes and underscores.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        },
        "sidebarmenu": {
            "dashboard": "Dashboard",
            "workouts": "Workouts",
            "bodysize": "Body Size",
            "progress_history": "Progress history",
            "meal": "Meal",
            "sleep": "Sleep",
            "muscles": "Muscles"
        }
    },
    "ru": {
        "passwords": {
            "password": "Пароль должен быть не менее шести символов и совпадать с подтверждением.",
            "reset": "Ваш пароль был сброшен!",
            "sent": "Ссылка на сброс пароля была отправлена!",
            "token": "Ошибочный код сброса пароля.",
            "user": "Не удалось найти пользователя с указанным электронным адресом."
        },
        "auth": {
            "failed": "Имя пользователя и пароль не совпадают.",
            "throttle": "Слишком много попыток входа. Пожалуйста, попробуйте еще раз через {seconds} секунд.",
            "make_auth": "Авторизуйтесь",
            "authorization": "Авторизация",
            "login": "Войти",
            "logout": "Выйти",
            "login_field": "Логин",
            "password_field": "Пароль",
            "back_to_the_main_page": "Вернуться на главную страницу"
        },
        "pagination": {
            "previous": "« Сюда",
            "next": "Туда »"
        },
        "validations": {
            "accepted": "Вы должны принять {attribute}.",
            "active_url": "Поле {attribute} содержит недействительный URL.",
            "after": "В поле {attribute} должна быть дата после {date}.",
            "after_or_equal": "В поле {attribute} должна быть дата после или равняться {date}.",
            "alpha": "Поле {attribute} может содержать только буквы.",
            "alpha_dash": "Поле {attribute} может содержать только буквы, цифры и дефис.",
            "alpha_num": "Поле {attribute} может содержать только буквы и цифры.",
            "array": "Поле {attribute} должно быть массивом.",
            "before": "В поле {attribute} должна быть дата до {date}.",
            "before_or_equal": "В поле {attribute} должна быть дата до или равняться {date}.",
            "between": {
                "numeric": "Поле {attribute} должно быть между {min} и {max}.",
                "file": "Размер файла в поле {attribute} должен быть между {min} и {max} Килобайт(а).",
                "string": "Количество символов в поле {attribute} должно быть между {min} и {max}.",
                "array": "Количество элементов в поле {attribute} должно быть между {min} и {max}."
            },
            "boolean": "Поле {attribute} должно иметь значение логического типа.",
            "confirmed": "Поле {attribute} не совпадает с подтверждением.",
            "date": "Поле {attribute} не является датой.",
            "date_format": "Поле {attribute} не соответствует формату {format}.",
            "different": "Поля {attribute} и {other} должны различаться.",
            "digits": "Длина цифрового поля {attribute} должна быть {digits}.",
            "digits_between": "Длина цифрового поля {attribute} должна быть между {min} и {max}.",
            "dimensions": "Поле {attribute} имеет недопустимые размеры изображения.",
            "distinct": "Поле {attribute} содержит повторяющееся значение.",
            "email": "Поле {attribute} должно быть действительным электронным адресом.",
            "file": "Поле {attribute} должно быть файлом.",
            "filled": "Поле {attribute} обязательно для заполнения.",
            "exists": "Выбранное значение для {attribute} некорректно.",
            "image": "Поле {attribute} должно быть изображением.",
            "in": "Выбранное значение для {attribute} ошибочно.",
            "in_array": "Поле {attribute} не существует в {other}.",
            "integer": "Поле {attribute} должно быть целым числом.",
            "ip": "Поле {attribute} должно быть действительным IP-адресом.",
            "ipv4": "Поле {attribute} должно быть действительным IPv4-адресом.",
            "ipv6": "Поле {attribute} должно быть действительным IPv6-адресом.",
            "json": "Поле {attribute} должно быть JSON строкой.",
            "max": {
                "numeric": "Поле {attribute} не может быть более {max}.",
                "file": "Размер файла в поле {attribute} не может быть более {max} Килобайт(а).",
                "string": "Количество символов в поле {attribute} не может превышать {max}.",
                "array": "Количество элементов в поле {attribute} не может превышать {max}."
            },
            "mimes": "Поле {attribute} должно быть файлом одного из следующих типов: {values}.",
            "mimetypes": "Поле {attribute} должно быть файлом одного из следующих типов: {values}.",
            "min": {
                "numeric": "Поле {attribute} должно быть не менее {min}.",
                "file": "Размер файла в поле {attribute} должен быть не менее {min} Килобайт(а).",
                "string": "Количество символов в поле {attribute} должно быть не менее {min}.",
                "array": "Количество элементов в поле {attribute} должно быть не менее {min}."
            },
            "not_in": "Выбранное значение для {attribute} ошибочно.",
            "numeric": "Поле {attribute} должно быть числом.",
            "present": "Поле {attribute} должно присутствовать.",
            "regex": "Поле {attribute} имеет ошибочный формат.",
            "required": "Поле {attribute} обязательно для заполнения.",
            "required_if": "Поле {attribute} обязательно для заполнения, когда {other} равно {value}.",
            "required_unless": "Поле {attribute} обязательно для заполнения, когда {other} не равно {values}.",
            "required_with": "Поле {attribute} обязательно для заполнения, когда {values} указано.",
            "required_with_all": "Поле {attribute} обязательно для заполнения, когда {values} указано.",
            "required_without": "Поле {attribute} обязательно для заполнения, когда {values} не указано.",
            "required_without_all": "Поле {attribute} обязательно для заполнения, когда ни одно из {values} не указано.",
            "same": "Значение {attribute} должно совпадать с {other}.",
            "size": {
                "numeric": "Поле {attribute} должно быть равным {size}.",
                "file": "Размер файла в поле {attribute} должен быть равен {size} Килобайт(а).",
                "string": "Количество символов в поле {attribute} должно быть равным {size}.",
                "array": "Количество элементов в поле {attribute} должно быть равным {size}."
            },
            "string": "Поле {attribute} должно быть строкой.",
            "timezone": "Поле {attribute} должно быть действительным часовым поясом.",
            "unique": "Такое значение поля {attribute} уже существует.",
            "uploaded": "Загрузка поля {attribute} не удалась.",
            "url": "Поле {attribute} имеет ошибочный формат.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        },
        "sidebarmenu": {
            "dashboard": "Панель управления",
            "workouts": "Тренировки",
            "bodysize": "Размеры тела",
            "progress_history": "Прогресс",
            "meal": "Приём пищи",
            "sleep": "Режим сна",
            "muscles": "Мышцы"
        }
    }
}
