// /**
//  * First we will load all of this project's JavaScript dependencies which
//  * includes Vue and other libraries. It is a great starting point when
//  * building robust, powerful web applications using Vue and Laravel.
//  */

// require('./bootstrap');

// window.Vue = require('vue');

// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('test-component', require('./components/MyTestComponent.vue'));

// const app = new Vue({
//   el: '#app'
// });



import Vue from 'vue';
import VueInternationalization from 'vue-i18n';
import Locale from '../assets/js/vue-i18n-locales.generated';

import Langswitcher from './components/langswitcher.vue';
// import example from './components/ExampleComponent.vue';
// import sidebar from './components/sidebarmenu.vue';

import Logout from './components/Languages/User/Profile.vue';


Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);
const langname = 'Русский';
const langimg = '/img/icons/flags/ru.svg';
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

const g_lang = new Vue({
    el: '#change-language',
    i18n,
    components: {
      Langswitcher,
      langname,
      langimg,
    }
});

const app = new Vue({
  el: '#app',
  i18n,
  components: {
    Logout,
  }
});
