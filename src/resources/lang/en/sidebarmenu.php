<?php

return [
    'dashboard' => 'Dashboard',
    'workouts' => 'Workouts',
    'bodysize' => 'Body Size',
    'progress_history' => 'Progress history',
    'meal' => 'Meal',
    'sleep' => 'Sleep',
    'muscles' => 'Muscles',
];
