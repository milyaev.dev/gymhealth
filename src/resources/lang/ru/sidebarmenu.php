<?php

return [
    'dashboard' => 'Панель управления',
    'workouts' => 'Тренировки',
    'bodysize' => 'Размеры тела',
    'progress_history' => 'Прогресс',
    'meal' => 'Приём пищи',
    'sleep' => 'Режим сна',
    'muscles' => 'Мышцы',
];
