@extends('layouts.main')

@section('content')
<div class="row">
    <main class="container" role="main">
        <div class="jumbotron">
            <h3>Welcome</h3>
            <h1>Начать открывать для себя новые возможности</h1>
            <p class="lead">This example is a quick exercise to illustrate how fixed to top navbar works. As you scroll, it will remain fixed to the top of your browser’s viewport.</p>

            <hr>
            <div class="row">
                <div class="container">
                    <a href="{{route('signUp')}}" class="btn btn-primary btn-lg">Попробовать сейчас</a>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="container">
                    <a href="{{route('login')}}" class="btn btn-outline-primary">У меня есть аккаунт</a>
                </div>
            </div>
        </div>
    </main>
</div>



@endsection
