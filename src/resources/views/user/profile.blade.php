@extends('layouts.main')

@section('content')

<h1 class="mt-0">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h1>
<a class="btn btn-color-white-blue" href="{{ route('user.profile.edit') }}">Настроить профиль</a>


<a class="btn btn-color-red" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <i class="feather icon-power text-danger"></i> <logout></logout>
</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

<p>
    Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Жаренные что правилами силуэт семь оксмокс дал себя его взобравшись, ведущими свой щеке. Парадигматическая алфавит диких точках, взобравшись текст напоивший.
</p>
<img class="icon" src="/img/icons/girl-run.svg">




@endsection
