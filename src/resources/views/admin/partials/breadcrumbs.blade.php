@if (count($breadcrumbs))

    <h4 class="font-weight-bold py-3 mb-0 mt-3">
      @foreach ($breadcrumbs as $breadcrumb)
        @if ($breadcrumb->url && $loop->last)
          {{ $breadcrumb->title }}
        @endif
      @endforeach
    </h4>
    <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
        <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="feather icon-home"></i></a></li>

            @foreach ($breadcrumbs as $breadcrumb)
              @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}"> {{ $breadcrumb->title }}</a></li>
              @else
                <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
              @endif
            @endforeach
        </ol>
    </div>
@endif
