@extends('layouts.admin')

@section('content')

<div class="media align-items-center py-3 mb-3">
    @if ($user->noPhoto())
    <img src="{{ URL::asset('/img/no-avatar.jpg') }}" class="d-block ui-w-100 rounded-circle ticket-assignee" alt="" title="" data-original-title="{{ $user->login }}">
    @else
      <img src="{{ URL::asset( $user->photo_original ) }}" class="d-block ui-w-100 rounded-circle ticket-assignee" alt="" title="" data-original-title="{{ $user->login }}">
    @endif
    <div class="media-body ml-4">
        <h4 class="font-weight-bold mb-0">{{ $user->firstname }} {{ $user->lastname }}<span class="text-muted font-weight-normal"> {{ '@'.$user->login }}</span></h4>
        <div class="text-muted mb-2">ID: {{ $user->id }}</div>
        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary btn-sm">Изменить</a>&nbsp;
        <div class="btn-group">
            <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"> <span>Настройки</span>&nbsp;</button>
            <div class="dropdown-menu">

                @if ($user->isUser())
                  <a href="{{ route('admin.users.set.trainer', $user) }}" class="dropdown-item text-primary">
                    <i class="ion ion-md-fitness"></i> &nbsp; Сделать тренером
                  </a>
                @endif

                @if ($user->isTrainer())
                  <a href="{{ route('admin.users.set.user', $user) }}" class="dropdown-item text-primary">
                    <i class="feather icon-user"></i> &nbsp; Сделать пользователем
                  </a>
                @endif

                @if ($user->isWait())
                <a href="{{ route('admin.users.verify', $user) }}" class="dropdown-item text-success">
                    <i class="feather icon-check-square text-success"></i> &nbsp; Verify</a>
                @endif

                @if (!$user->noPhoto())
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('admin.users.photo.delete', $user) }}">
                      <i class="feather icon-image text-danger"></i> &nbsp; {{ __('Удалить фото') }}
                  </a>
                @endif

                @if ($user->isBanned())
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('admin.users.set.wait', $user) }}">
                      <i class="feather icon-user-x text-success"></i> &nbsp; {{ __('Разблокировать') }}
                  </a>
                @endif

                @if ($user->isActive())
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('admin.users.set.banned', $user) }}">
                      <i class="feather icon-user-x text-danger"></i> &nbsp; {{ __('Заблокировать') }}
                  </a>

                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('admin.users.destroy', $user) }}">
                      <i class="feather icon-x-circle text-danger"></i> &nbsp; {{ __('Удалить') }}
                  </a>
                @endif
            </div>
        </div>&nbsp;
        <a href="javascript:void(0)" class="btn btn-default btn-sm icon-btn">
            <i class="ion ion-md-mail"></i>
        </a>
    </div>
</div>

<div class="card mb-4">
    <div class="card-body">
        <table class="table user-view-table m-0">
            <tbody>
                <tr>
                    <td>Email:</td>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <td>Регистрация:</td>
                    <td>{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                </tr>
                <tr>
                    <td>Активность:</td>
                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                </tr>
                <tr>
                    <td>Verified:</td>
                    <td>
                        @if ($user->isWait())
                            <span class="ion ion-md-time text-primary"></span>&nbsp; No Verify
                        @else
                          <span class="ion ion-md-checkmark text-primary"></span>&nbsp; Yes
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Роль:</td>
                    <td>
                        @if ($user->isAdmin())
                            <span class="badge badge-success">Администратор</span>
                        @endif
                        @if ($user->isModer())
                            <span class="badge badge-warning">Модератор</span>
                        @endif
                        @if ($user->isTrainer())
                            <span class="badge badge-info">Тренер</span>
                        @endif
                        @if ($user->isUser())
                            <span class="badge badge-secondary">Пользователь</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Статус:</td>
                    <td>
                        @if ($user->isWait())
                            <span class="badge badge-outline-secondary">Waiting</span>
                        @endif
                        @if ($user->isActive())
                            <span class="badge badge-outline-success">Active</span>
                        @endif
                        @if ($user->isBanned())
                            <span class="badge badge-outline-danger">Banned</span>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="row no-gutters row-bordered">
        <div class="d-flex col-md align-items-center">
            <a href="javascript:void(0)" class="card-body d-block text-dark">
                <div class="text-muted small line-height-1">Упражнений использует</div>
                <div class="text-xlarge">125</div>
            </a>
        </div>
        <div class="d-flex col-md align-items-center">
            <a href="javascript:void(0)" class="card-body d-block text-dark">
                <div class="text-muted small line-height-1">Тренировок использует</div>
                <div class="text-xlarge">24</div>
            </a>
        </div>
        <div class="d-flex col-md align-items-center">
            <a href="javascript:void(0)" class="card-body d-block text-dark">
                <div class="text-muted small line-height-1">Подписчиков</div>
                <div class="text-xlarge">306</div>
            </a>
        </div>
    </div>
    <hr class="border-light m-0">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table user-view-table m-0">
                <tbody>
                    <tr>
                        <td>Логин:</td>
                        <td>{{ $user->login }}</td>
                    </tr>
                    <tr>
                        <td>Имя Фамилия:</td>
                        <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                    </tr>
                    <tr>
                        <td>E-mail:</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h6 class="mt-4 mb-3">Social links</h6>
        <div class="table-responsive">
            <table class="table user-view-table m-0">
                <tbody>
                    <tr>
                        <td>Twitter:</td>
                        <td>
                            <a href="javascript:void(0)">https://twitter.com/user</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Facebook:</td>
                        <td>
                            <a href="javascript:void(0)">https://www.facebook.com/user</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Instagram:</td>
                        <td>
                            <a href="javascript:void(0)">https://www.instagram.com/user</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h6 class="mt-4 mb-3">Personal info</h6>
        <div class="table-responsive">
            <table class="table user-view-table m-0">
                <tbody>
                    <tr>
                        <td>Birthday:</td>
                        <td>May 3, 1995</td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td>Canada</td>
                    </tr>
                    <tr>
                        <td>Languages:</td>
                        <td>English</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h6 class="mt-4 mb-3">Contacts</h6>
        <div class="table-responsive">
            <table class="table user-view-table m-0">
                <tbody>
                    <tr>
                        <td>Phone:</td>
                        <td>+0 (123) 456 7891</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h6 class="mt-4 mb-3">Interests</h6>
        <div class="table-responsive">
            <table class="table user-view-table m-0">
                <tbody>
                    <tr>
                        <td>Favorite music:</td>
                        <td>
                            Rock, Alternative, Electro, Drum &amp; Bass, Dance
                        </td>
                    </tr>
                    <tr>
                        <td>Favorite movies:</td>
                        <td>
                            The Green Mile, Pulp Fiction, Back to the Future, WALL·E, Django Unchained
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
