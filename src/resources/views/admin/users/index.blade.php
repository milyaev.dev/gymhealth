@extends('layouts.admin')

@section('content')

<p><a href="{{ route('admin.users.create') }}" class="btn btn-success mb-2">Добавить нового</a></p>

<div class="card">
    <div class="card-header">Список всех пользователей</div>
    <table class="table card-table table-hover">
        <div class="container-fluid flex-grow-1 container-p-y">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">ID</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Login</label>
                            <input id="name" class="form-control" name="login" value="{{ request('login') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">Status</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $value => $label)
                                    <option value="{{ $value }}"{{ $value === request('status') ? ' selected' : '' }}>{{ $label }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="role" class="col-form-label">Role</label>
                            <select id="role" class="form-control" name="role">
                                <option value=""></option>
                                @foreach ($roles as $value => $label)
                                    <option value="{{ $value }}"{{ $value === request('role') ? ' selected' : '' }}>{{ $label }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>Email</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($users as $user)
                <tr>
                    <th scope="row">
                        <div class="media align-items-center p-2">
                          {{ $user->id }}
                        </div>
                    </th>

                      <td>
                        <a href="{{ route('admin.users.show', $user) }}" style="color:#212529">

                          <div class="media align-items-center pb-1">
                              @if ($user->noPhoto())
                                <img src="{{ URL::asset('/img/no-avatar.jpg') }}" class="ui-w-40 rounded-circle" alt="">
                              @else
                                <img src="{{ URL::asset( $user->photo_original ) }}" class="ui-w-40 rounded-circle" alt="">
                              @endif
                              <div class="media-body ml-3">
                                  <a href="{{ route('admin.users.show', $user) }}" class="text-dark">{{ $user->firstname }} {{ $user->lastname }}</a>
                                  <div class="text-muted small">@ {{ $user->login }}</div>
                              </div>
                              @if ($user->isAdmin())
                                <a href="users?role=admin" class="btn btn-sm btn-success d-block"><span class="fa fa-user-astronaut"></span> Администратор</a>
                              @endif
                              @if ($user->isModer())
                                <a href="users?role=moder" class="btn btn-sm btn-warning d-block"><span class="ion ion-logo-octocat"></span> Модератор</a>
                              @endif
                              @if ($user->isTrainer())
                                <a href="users?role=trainer" class="btn btn-sm btn-info d-block"><span class="ion ion-md-fitness"></span>  Тренер</a>
                              @endif
                          </div>




                      </a>
                      </td>
                    <td>
                      <div class="media align-items-center p-2">
                        {{ $user->email }}
                      </div>
                    </td>
                    <td>
                      <div class="media align-items-center p-2">
                        @if ($user->isWait())
                            <a href="users?status=wait" class="btn btn-sm btn-secondary"><span class="fa fas fa-clock"></span> Waiting</a>
                        @endif
                        @if ($user->isBanned())
                            <a href="users?status=banned" class="btn btn-sm btn-danger"><span class="fa fas fa-user-slash"></span> Banned</a>
                        @endif
                      </div>
                    </td>
                    <td class="text-center text-nowrap">
                      <div class="media align-items-center p-2">
                        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-default btn-xs icon-btn md-btn-flat user-tooltip" title="" data-original-title="Edit">
                          <i class="ion ion-md-create"></i>
                        </a>&nbsp;&nbsp;
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-xs icon-btn md-btn-flat dropdown-toggle hide-arrow user-tooltip" title="" data-toggle="dropdown" data-original-title="Actions">
                            <i class="ion ion-ios-settings"></i>
                          </button>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('admin.users.show', $user) }}">View profile</a>
                            <a class="dropdown-item" href="javascript:void(0)">Ban user</a>
                            <a class="dropdown-item" href="javascript:void(0)">Remove</a>
                          </div>
                        </div>
                      </div>
                    </td>
                </tr>
            @endforeach

        </tbody>

        {{ $users->links() }}

    </table>
</div>
<br><br>

@endsection
