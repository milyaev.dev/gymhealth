@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- 2nd row Start -->
        <div class="col-md-12">
            <div class="card d-flex w-100 mb-4">
                <div class="row no-gutters row-bordered row-border-light h-100">
                    <div class="d-flex col-md-6 col-lg-3 align-items-center">
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col-auto">
                                    <i class="lnr lnr-users text-success display-4"></i>
                                </div>
                                <div class="col">
                                    <h6 class="mb-0 text-muted"><span class="text-success">Активных</span> пользователей</h6>
                                    <h4 class="mt-3 mb-0"><span class="text-success">{{ $active_users }}</span> из {{ $goal_users }}</h4>
                                    <div class="text-right">
                                        <p class="mb-0 text-muted">До рекорда: <span class="text-success">{{ $need_users }}</span> чел.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="progress mt-1">
                                <div class="progress-bar bg-success" style="width:{{ $procent_users }}%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex col-md-6 col-lg-3 align-items-center">
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col-auto">
                                    <i class="lnr lnr-database text-primary display-4"></i>
                                </div>
                                <div class="col">
                                    <h6 class="mb-0 text-muted"><span class="text-primary">Нагрузка </span>на сервер</h6>
                                    <h4 class="mt-3 mb-0">{{ $sysload }}<i class="ion ion-md-arrow-round-down ml-3 text-danger"></i></h4>
                                </div>
                            </div>
                            <p class="mb-0 text-muted">Среднее значение за Now / 1 min / 15 min</p>
                        </div>
                    </div>
                    <div class="d-flex col-md-6 col-lg-3 align-items-center">
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col-auto">
                                    <i class="lnr lnr-cart text-primary display-4"></i>
                                </div>
                                <div class="col">
                                    <h6 class="mb-0 text-muted"><span class="text-primary">Order</span> Status</h6>
                                    <h4 class="mt-3 mb-0">6325<i class="ion ion-md-arrow-round-up ml-3 text-success"></i></h4>
                                </div>
                            </div>
                            <p class="mb-0 text-muted">36% From Last 6 Months</p>
                        </div>
                    </div>

                    <div class="d-flex col-md-6 col-lg-3 align-items-center">
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col-auto">
                                    <i class="lnr lnr-cart text-primary display-4"></i>
                                </div>
                                <div class="col">
                                    <h6 class="mb-0 text-muted"><span class="text-primary">Order</span> Status</h6>
                                    <h4 class="mt-3 mb-0">6325<i class="ion ion-md-arrow-round-up ml-3 text-success"></i></h4>
                                </div>
                            </div>
                            <p class="mb-0 text-muted">36% From Last 6 Months</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Staustic card 3 Start -->
        </div>
        <!-- 2nd row Start -->
    </div>

@endsection
