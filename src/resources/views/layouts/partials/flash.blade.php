@if (session('status'))
  <div class="alert alert-dark-primary alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {{ session('status') }}
  </div>
@endif

@if (session('success'))
  <div class="alert alert-dark-success alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {{ session('success') }}
  </div>
@endif

@if (session('error'))
  <div class="alert alert-dark-danger alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {{ session('error') }}
  </div>
@endif

@if (session('info'))
  <div class="alert alert-dark-secondary alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {{ session('info') }}
  </div>
@endif
