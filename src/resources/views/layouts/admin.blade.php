<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dashboard - {{ config('app.name', 'Laravel') }}</title>

    <!-- Favicons -->

    <link rel="apple-touch-icon" sizes="180x180" href="/admin/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/admin/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/admin/img/favicons/site.webmanifest">
    <link rel="mask-icon" href="/admin/img/favicons/safari-pinned-tab.svg" color="#303034">
    <meta name="apple-mobile-web-app-title" content="GymHealth">
    <meta name="application-name" content="GymHealth">
    <meta name="msapplication-TileColor" content="#303034">
    <meta name="theme-color" content="#ffffff">

    <!-- Scripts -->

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="chromesniffer" id="chromesniffer_meta" content="{&quot;SPDY&quot;:-1,&quot;Bootstrap&quot;:-1}"><script type="text/javascript" src="chrome-extension://fhhdlnnepfjhlhilgmeepgkhjmhhhjkh/js/detector.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <!-- Styles -->


    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="/admin/fonts/fontawesome.css">
    <link rel="stylesheet" href="/admin/fonts/ionicons.css">
    <link rel="stylesheet" href="/admin/fonts/linearicons.css">
    <link rel="stylesheet" href="/admin/fonts/open-iconic.css">
    <link rel="stylesheet" href="/admin/fonts/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/admin/fonts/feather.css">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="/admin/css/bootstrap-material2.css">
    <link rel="stylesheet" href="/admin/css/shreerang-material.css">
    <link rel="stylesheet" href="/admin/css/uikit.css">

    <link rel="stylesheet" href="/admin/css/pages/users.css">
    <link rel="stylesheet" href="/admin/css/pages/select2.css">
    <link rel="stylesheet" href="/admin/css/bootstrap-tagsignput.css">

    <!-- Libs -->
    <link rel="stylesheet" href="/admin/libs/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="/admin/libs/flot/flot.css">


</head>


<body>
    <!-- [ Preloader ] Start -->
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>
    <!-- [ Preloader ] End -->

    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-dark">
                <!-- Brand demo (see assets/css/demo/demo.css) -->
                <div class="app-brand demo">
                    <span class="app-brand-logo">
                            <a href="{{ route('admin.home') }}"><img src="{{ URL::asset('/img/logo-white.svg') }}"  width="155" style="padding: 17px 0;" alt="Admin panel"></a>
                    </span>
                    <a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                        <i class="ion ion-md-menu align-middle"></i>
                    </a>
                </div>
                <div class="sidenav-divider mt-0"></div>

                <!-- Links -->
                <ul class="sidenav-inner py-1">

                    <!-- Dashboards -->
                    <li class="sidenav-item {{ Request::is('admin/home') ? 'active' : '' }}">
                        <a href="{{ route('admin.home')}}" class="sidenav-link {{ Request::is('admin/home'.'*') ? 'active' : '' }}">
                            <i class="sidenav-icon feather icon-home"></i>
                            <div>Dashboard</div>
                            <div class="pl-1 ml-auto">
                                <div class="badge badge-pill badge-primary"><i class="feather icon-sliders"></i></div>
                            </div>
                        </a>
                    </li>

                    <!-- Layouts -->
                    <li class="sidenav-divider mb-1"></li>
                    <li class="sidenav-header small font-weight-semibold">Основные</li>
                    <li class="sidenav-item">
                        <a href="typography.html" class="sidenav-link">
                            <i class="sidenav-icon feather icon-message-square"></i>
                            <div>Отзывы</div>
                        </a>
                    </li>

                    <!-- Users -->
                    <li class="sidenav-item {{ Request::is('admin/users'. '*') ? 'open' : '' }}">
                        <a href="javascript:" class="sidenav-link sidenav-toggle">
                            <i class="sidenav-icon feather icon-users"></i>
                            <div>Пользователи</div>
                        </a>
                        <ul class="sidenav-menu">
                            <li class="sidenav-item {{ Request::is('admin/user/?status=active') ? 'active' : '' }}">
                                <a href="{{ route('admin.users.index')}}/?status=active" class="sidenav-link">
                                    <i class="sidenav-icon ion ion-ios-people"></i>
                                    <div>Пользователи</div>
                                </a>
                            </li>
                            <li class="sidenav-item {{ Request::is('admin/users/create') ? 'active' : '' }}">
                                <a href="{{ route('admin.users.create')}}" class="sidenav-link">
                                    <i class="sidenav-icon ion ion-md-person-add"></i>
                                    <div>Создать </div>
                                </a>
                            </li>
                            <li class="sidenav-item {{ Request::is('admin/users') ? 'active' : '' }}">
                                <a href="{{ route('admin.users.index')}}" class="sidenav-link">
                                    <i class="sidenav-icon ion ion-ios-people"></i>
                                    <div>Все пользователи</div>
                                </a>
                            </li>
                        </ul>
                    </li>

                     <!-- Trainers -->
                    <li class="sidenav-item">
                        <a href="javascript:" class="sidenav-link sidenav-toggle">
                            <i class="sidenav-icon ion ion-ios-fitness"></i>
                            <div>Тренера</div>
                        </a>
                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="bui_badges.html" class="sidenav-link">
                                    <i class="sidenav-icon ion ion-ios-filing"></i>
                                    <div>Заявки</div>
                                    <div class="pl-1 ml-auto">
                                        <div class="badge badge-pill badge-primary">2</div>
                                    </div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="{{ route('admin.users.index')}}?role=trainer" class="sidenav-link">
                                    <i class="sidenav-icon ion ion-ios-people"></i>
                                    <div>Список тренеров</div>
                                </a>
                            </li>

                        </ul>
                    </li>

                </ul>
            </div>
            <!-- [ Layout sidenav ] End -->
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] Start -->
                <nav class="layout-navbar navbar navbar-expand-lg align-items-lg-center bg-white container-p-x" id="layout-navbar">

                    <!-- Brand demo (see assets/css/demo/demo.css) -->
                    <a href="index.html" class="navbar-brand app-brand demo d-lg-none py-0 mr-4">
                        <span class="app-brand-logo demo">
                            <img src="{{ URL::asset('/img/logo-white.svg') }}" alt="Brand Logo" class="img-fluid">
                        </span>
                        <span class="app-brand-text demo font-weight-normal ml-2">Bhumlu</span>
                    </a>

                    <!-- Sidenav toggle (see assets/css/demo/demo.css) -->
                    <div class="layout-sidenav-toggle navbar-nav d-lg-none align-items-lg-center mr-auto">
                        <a class="nav-item nav-link px-0 mr-lg-4" href="javascript:">
                            <i class="ion ion-md-menu text-large align-middle"></i>
                        </a>
                    </div>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#layout-navbar-collapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="navbar-collapse collapse" id="layout-navbar-collapse">
                        <!-- Divider -->
                        <hr class="d-lg-none w-100 my-2">

                        <div class="navbar-nav align-items-lg-center">
                            <!-- Search -->
                            <label class="nav-item navbar-text navbar-search-box p-0 active">
                                <i class="feather icon-search navbar-icon align-middle"></i>
                                <span class="navbar-search-input pl-2">
                                    <input type="text" class="form-control navbar-text mx-2" placeholder="Search...">
                                </span>
                            </label>
                        </div>

                        <div class="navbar-nav align-items-lg-center ml-auto">
                            <div class="demo-navbar-notifications nav-item dropdown mr-lg-3">
                                <a class="nav-link dropdown-toggle hide-arrow" href="#" data-toggle="dropdown">
                                    <i class="feather icon-bell navbar-icon align-middle"></i>
                                    {{-- <span class="badge badge-danger badge-dot indicator"></span> --}}
                                    <span class="d-lg-none align-middle">&nbsp; Notifications</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="bg-primary text-center text-white font-weight-bold p-3">
                                        4 New Notifications
                                    </div>
                                    <div class="list-group list-group-flush">
                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-home bg-secondary border-0 text-white"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">Login from 192.168.1.1</div>
                                                <div class="text-light small mt-1">
                                                    Aliquam ex eros, imperdiet vulputate hendrerit et.
                                                </div>
                                                <div class="text-light small mt-1">12h ago</div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-user-plus bg-info border-0 text-white"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">You have
                                                    <strong>4</strong> new followers</div>
                                                <div class="text-light small mt-1">
                                                    Phasellus nunc nisl, posuere cursus pretium nec, dictum vehicula tellus.
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-power bg-danger border-0 text-white"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">Server restarted</div>
                                                <div class="text-light small mt-1">
                                                    19h ago
                                                </div>
                                            </div>
                                        </a>

                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <div class="ui-icon ui-icon-sm feather icon-alert-triangle bg-warning border-0 text-dark"></div>
                                            <div class="media-body line-height-condenced ml-3">
                                                <div class="text-dark">99% server load</div>
                                                <div class="text-light small mt-1">
                                                    Etiam nec fringilla magna. Donec mi metus.
                                                </div>
                                                <div class="text-light small mt-1">
                                                    20h ago
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <a href="javascript:" class="d-block text-center text-light small p-2 my-1">Show all notifications</a>
                                </div>
                            </div>

                            <div class="demo-navbar-messages nav-item dropdown mr-lg-3">
                                <a class="nav-link dropdown-toggle hide-arrow" href="#" data-toggle="dropdown">
                                    <i class="feather icon-mail navbar-icon align-middle"></i>
                                    <span class="badge badge-success badge-dot indicator"></span>
                                    <span class="d-lg-none align-middle">&nbsp; Messages</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="bg-primary text-center text-white font-weight-bold p-3">
                                        4 New Messages
                                    </div>
                                    <div class="list-group list-group-flush">
                                        <a href="javascript:" class="list-group-item list-group-item-action media d-flex align-items-center">
                                            <img src="/admin/img/avatars/6-small.png" class="d-block ui-w-40 rounded-circle" alt>
                                            <div class="media-body ml-3">
                                                <div class="text-dark line-height-condenced">Lorem ipsum dolor consectetuer elit.</div>
                                                <div class="text-light small mt-1">
                                                    Josephin Doe &nbsp;·&nbsp; 58m ago
                                                </div>
                                            </div>
                                        </a>

                                    </div>

                                    <a href="javascript:" class="d-block text-center text-light small p-2 my-1">Show all messages</a>
                                </div>
                            </div>

                            <!-- Link to WebSite -->

                            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|</div>
                            <div class="nav-item mr-lg-3">
                                <a href="{{ route('home')}}" class="nav-link" target="_blank">
                                    <i class="feather icon-monitor navbar-icon align-middle"></i>
                                </a>
                            </div>

                            <!-- Divider -->
                            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|</div>
                            <div class="demo-navbar-user nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                                    <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                                        @if ( Auth::user()->photo_original == NULL )
                                          <img src="{{ URL::asset('/img/no-avatar.jpg') }}" class="d-block ui-w-30 rounded-circle" alt="{{ Auth::user()->login }}">
                                        @else
                                          <img src="{{ URL::asset(Auth::user()->photo_original) }}" class="d-block ui-w-30 rounded-circle " alt="{{ Auth::user()->login }}">
                                        @endif
                                        <span class="px-1 mr-lg-2 ml-2 ml-lg-0">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</span>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/admin/users/{{  Auth::user()->id }}" class="dropdown-item">
                                        <i class="feather icon-user text-muted"></i> &nbsp; My profile</a>
                                    <a href="javascript:" class="dropdown-item disable">
                                        <i class="feather icon-mail text-muted"></i> &nbsp; Messages</a>
                                    <a href="javascript:" class="dropdown-item">
                                        <i class="feather icon-settings text-muted"></i> &nbsp; Account settings</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                      onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        <i class="feather icon-power text-danger"></i> &nbsp; {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- [ Layout navbar ( Header ) ] End -->

                <!-- [ Layout content ] Start -->
                <div class="layout-content">
                    <div class=container-fluid flex-grow-1 container-p-y">
                        @section('breadcrumbs', Breadcrumbs::render() )
                        @yield('breadcrumbs')
                        @include('layouts.partials.flash')
                        @yield('content')
                    </div>
                    <!-- [ content ] End -->

                    <!-- [ Layout footer ] Start -->
                    <nav class="layout-footer footer bg-white">
                        <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
                            <div class="pt-3">
                            <span class="footer-text font-weight-semibold">&copy; <a href="{{ route('home') }}" class="footer-link" target="_blank">GymHealth</a></span>
                            </div>
                            <div>
                                <a href="javascript:" class="footer-link pt-3">About Us</a>
                                <a href="javascript:" class="footer-link pt-3 ml-4">Help</a>
                                <a href="javascript:" class="footer-link pt-3 ml-4">Contact</a>
                                <a href="javascript:" class="footer-link pt-3 ml-4">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </nav>
                    <!-- [ Layout footer ] End -->
                </div>
                <!-- [ Layout content ] Start -->
            </div>
            <!-- [ Layout container ] End -->
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->

    <!-- Core scripts -->
    <script src="/admin/js/pace.js"></script>
    <script src="/admin/js/jquery-3.2.1.min.js"></script>
    <script src="/admin/libs/popper/popper.js"></script>
    <script src="/admin/js/bootstrap.js"></script>
    <script src="/admin/js/sidenav.js"></script>
    <script src="/admin/js/layout-helpers.js"></script>
    <script src="/admin/js/material-ripple.js"></script>

    <!-- Libs -->
    <script src="/admin/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/admin/libs/eve/eve.js"></script>
    <script src="/admin/libs/flot/flot.js"></script>
    <script src="/admin/libs/flot/curvedLines.js"></script>
    <script src="/admin/libs/chart-am4/core.js"></script>
    <script src="/admin/libs/chart-am4/charts.js"></script>
    <script src="/admin/libs/chart-am4/animated.js"></script>

    <!-- Demo -->
    <script src="/admin/js/demo.js"></script>
    <script src="/admin/js/analytics.js"></script>
    <script src="/admin/js/pages/dashboards_index.js"></script>
</body>

</html>
