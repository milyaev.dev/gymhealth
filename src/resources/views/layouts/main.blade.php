<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

<div class="grid-wrapper">
  <div class="wrapper-header">

    <nav class="navbar">
      <ul class="top-menu">
        <li class="nav-item">
            <a class="nav-link" href="{{route('event.workout')}}">Тренировки</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('event.meal')}}">Приём пищи</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('event.sleep')}}">Режим сна</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('event.sleep')}}">Мышцы</a>
        </li>
      </ul>
    </nav>

    <!-- Profile User -->
    <div class="profile">
      @guest
        <div class="login">
          <a class="btn btn-start" href="{{ route('welcome') }}">{{ __("Let's Start") }}</a>
        </div>
      @else
      <a href="{{ route('user.profile') }}" class="profile-user">
          <div class="profile-user-name">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</div>
          <div class="profile-user-photo">
              <img src="{{ URL::asset(  Auth::user()->photo_original ) }}" class="userphoto">
              @if ( Auth::user()->isTrainer() )
                <span class="badge badge-small badge-blue role role-left">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.7556 4.30061L12.598 3.14293L13.4682 2.27299C13.5452 2.19612 13.5884 2.09197 13.5884 1.98303C13.5884 1.8741 13.5452 1.76994 13.4683 1.69305L12.3031 0.527879C12.1493 0.37407 11.8769 0.37407 11.7231 0.527879L10.8531 1.39815L9.6953 0.24025C9.37497 -0.0800832 8.85557 -0.0800832 8.53521 0.24025C8.21483 0.560582 8.21483 1.07998 8.53521 1.40034L12.5955 5.4607C12.9159 5.78103 13.4353 5.78103 13.7556 5.4607C14.076 5.14037 14.076 4.62097 13.7556 4.30061Z" fill="white"/>
                      <path d="M5.46556 12.5994L1.40525 8.53908C1.08486 8.21875 0.56552 8.21875 0.245132 8.53908C-0.0752004 8.85946 -0.0752004 9.37886 0.245132 9.69919L1.40301 10.8571L0.53443 11.7253C0.457512 11.8022 0.414254 11.9063 0.414254 12.0153C0.414254 12.1242 0.457512 12.2284 0.53443 12.3052L1.6996 13.47C1.77969 13.5502 1.88464 13.5902 1.98959 13.5902C2.09453 13.5902 2.19948 13.5502 2.27957 13.4701L3.14779 12.6018L4.30547 13.7595C4.6258 14.0799 5.1452 14.0799 5.46556 13.7595C5.78594 13.4392 5.78594 12.9198 5.46556 12.5994Z" fill="white"/>
                      <path d="M7.37486 4.88037L4.88477 7.37894L6.62493 9.1191L9.11502 6.62048L7.37486 4.88037Z" fill="white"/>
                      <path d="M12.5954 6.62086L7.37499 1.40041C7.05465 1.08007 6.53523 1.08007 6.2149 1.40041C5.89451 1.72079 5.89451 2.24013 6.2149 2.56052L11.4353 7.78097C11.7557 8.10131 12.2751 8.10131 12.5954 7.78097C12.9158 7.46064 12.9158 6.94125 12.5954 6.62086Z" fill="white"/>
                      <path d="M7.7849 11.4397L2.56442 6.21925C2.24408 5.89892 1.72469 5.89892 1.40435 6.21925C1.08397 6.53964 1.08397 7.05903 1.40435 7.37937L6.62481 12.5998C6.94515 12.9201 7.46454 12.9201 7.7849 12.5998C8.10529 12.2795 8.10529 11.7601 7.7849 11.4397Z" fill="white"/>
                    </svg>
                </span>
              @endif
              @if ( Auth::user()->isAdmin() )
                <span class="badge badge-small badge-red role role-left">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M2.12402 10.1167V11.057C2.12402 11.2847 2.30859 11.4693 2.53632 11.4693H11.4641C11.6918 11.4693 11.8763 11.2847 11.8763 11.057V10.1167H2.12402Z" fill="white"/>
                      <path d="M14 4.87584C14 4.1413 13.4024 3.54369 12.6678 3.54369C11.9333 3.54369 11.3358 4.1413 11.3358 4.87584C11.3358 5.22896 11.4741 5.55025 11.6993 5.78886C11.2988 6.23683 10.7174 6.51956 10.0707 6.51956C9.02397 6.51956 8.14726 5.77957 7.93513 4.79541C8.18005 4.5537 8.33215 4.2182 8.33215 3.84778C8.33215 3.11324 7.73454 2.51562 7 2.51562C6.26546 2.51562 5.66785 3.11324 5.66785 3.84778C5.66785 4.2182 5.82005 4.5537 6.06487 4.79541C5.85274 5.77957 4.97603 6.51956 3.92928 6.51956C3.28264 6.51956 2.70116 6.23683 2.30072 5.78886C2.52588 5.55025 2.66431 5.22896 2.66431 4.87584C2.66431 4.1413 2.0667 3.54369 1.33215 3.54369C0.59761 3.54369 0 4.1413 0 4.87584C0 5.51244 0.448929 6.04585 1.04675 6.1768C1.16499 7.15263 1.39346 8.34155 1.83459 9.29196H12.1654C12.6065 8.34155 12.835 7.15263 12.9532 6.1768C13.5511 6.04575 14 5.51244 14 4.87584Z" fill="white"/>
                    </svg>
                </span>
              @endif
          </div>
        </a>
      @endguest


    </div>
  </div>

  <div class="sidebar">
    <a class="logo" href="{{route('home')}}">
      <img class="icon" src="/img/logo.svg">
      <img class="name" src="/img/GymHealth.svg">
    </a>
    <nav>
      <ul class="menu">
          <li>
            <a class="text-muted" href="{{route('welcome')}}">
              <span class="icon">
                  <img src="/img/icons/calendar.svg">
              </span>
              <span class="name">План развития</span>
            </a>
          </li>
          <li>
            <a class="text-muted" href="{{route('login')}}">
              <span class="icon">
                <img src="/img/icons/girya.svg">
              </span>
              <span class="name">Тренировки</span>
            </a>
          </li>
          <li>
            <a class="text-muted" href="{{route('login')}}">
              <span class="icon">
                <img src="/img/icons/biceps.svg">
              </span>
              <span class="name">Объёмы тела</span>
            </a>
          </li>
          <li>
            <a class="text-muted active" href="{{route('login')}}">
              <span class="icon">
                <img src="/img/icons/meal.svg">
                <div class="notification">
                  <span class="badge badge-small badge-red badge-icon">3</span>
                </div>
              </span>
              <span class="name">Приём пищи</span>
            </a>
          </li>
          <li>
            <a class="text-muted" href="{{route('login')}}">
              <span class="icon">
                <img src="/img/icons/stat.svg">
              </span>
              <span class="name">Прогресс</span>
            </a>
          </li>
        @auth
          @if ( Auth::user()->isTrainer() )
            <li>
              <a class="text-muted" href="{{route('login')}}">
                <span class="icon">
                  <img src="/img/icons/biceps.svg">
                </span>
                <span class="name">Тренерство</span>
              </a>
            </li>
          @endif
        @endauth
      </ul>
    </nav>
  </div>

  <div class="content" id="app">
      @include('layouts.partials.flash')
      @yield('content')
  </div>

  <div class="footer">
    <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="{{route('welcome')}}">Welcome</a></li>
        <li><a class="text-muted" href="{{route('login')}}">Login</a></li>
        <li><a class="text-muted" href="{{route('signUp')}}">Sign Up</a></li>
        <li><a class="text-muted" href="{{route('forgetPassword')}}">Forget Password</a></li>
        <li><a class="text-muted" href="{{route('loginSocialMedia')}}">Login Social media</a></li>
        @guest
        @else
          @if ( Auth::user()->isAdmin() || Auth::user()->isModer() )
            <li><a class="text-muted" href="{{route('admin.home')}}">Admin Dashboard</a></li>
          @endif
        @endguest
    </ul>

    <div id="change-language">
      <Langswitcher></Langswitcher>
    </div>

    {{--

      <div class="lang">
      <ul class="select-lang">
        <li class="language">
          <img class="flag" src="/img/icons/flags/zh.svg">
          <span class="name">中国</span>
        </li>
        <li class="language">
          <img class="flag" src="/img/icons/flags/fr.svg">
          <span class="name">France</span>
        </li>
        <li class="language">
          <img class="flag" src="/img/icons/flags/pt.svg">
          <span class="name">Portugal</span>
        </li>
        <li class="language">
          <img class="flag" src="/img/icons/flags/ru.svg">
          <span class="name">Русский</span>
        </li>
        <li class="language">
          <img class="flag" src="/img/icons/flags/es.svg">
          <span class="name">Español</span>
        </li>
      </ul>
      <div class="active">
        <img class="flag flag-active" src="/img/icons/flags/us.svg">
        <span class="name">English</span>
        <img class="arrow" src="/img/icons/flags/arrow.svg">
      </div>
    </div>
  </div> --}}
  </div>

</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

</body>
</html>
