<?php

use Carbon\Carbon;
use App\Models\User\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

/** @var EloquentFactory $factory */

$factory->define(User::class, function (Faker $faker) {
    $active = $faker->randomElement([true, true, true, false]);
    $banned = $active ? $faker->randomElement([true, false, false]) : false;
    return [
        'login'          => $faker->unique()->userName,
        'email'          => $faker->unique()->email,
        'password'       => User::hashPassword('1234'),
        'remember_token' => Str::random(10),
        'firstname'      => $faker->unique()->firstName,
        'lastname'       => $faker->unique()->lastName,
        'photo_original' => 'img/test-avatar/'.rand(1,30).'.jpg',
        'role'           => $faker->randomElement(['user', 'user', 'user', 'trainer']),
        //'deleted_at'     => $faker->randomElement([null, null, null, Carbon::now()]),
        //'deleted_at'     => $banned ? Carbon::now() : null,
        'status'         => $active ? $banned ? User::STATUS_BANNED : User::STATUS_ACTIVE : User::STATUS_WAIT,
        'verify_token'   => $active ? null : Str::random(30),
    ];
});
