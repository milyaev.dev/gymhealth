<?php

use App\Models\User\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Array with users which must be created with static (fixed) logins and passwords. Key is login, value - password.
     *
     * @var array
     */

    protected $static_users = [
        'admin' => [
            'login' => 'admin',
            'password' => 'nhjcvfy12',
            'email' => 'isddxy@gmail.com',
            'role' => 'admin', //admin
            'status' => 'active',
            'firstname' => 'Anton',
            'lastname' => 'Milyaev',
            'avatar' => 'img/test-avatar/admin.jpg'
        ],
        'trainer' => [
            'login' => 'trainer',
            'password' => '12345678', //moder password
            'email' => 'isddxy2@gmail.com',
            'role' => 'moder', //moder
            'status' => 'active',
            'firstname' => 'Игорь',
            'lastname' => 'Жильцов',
            'avatar' => 'img/test-avatar/trainer.jpg'
        ],
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->static_users as $users => $user) {
            $this->createStaticUser($user['login'], $user['password'], $user['email'], $user['role'], $user['status'], $user['firstname'], $user['lastname'], $user['avatar']);
        }

        $this->seedModelsBatch();
    }

    public function setOriginalPhoto($user)
    {
        $user->photo_original = public_path('img/1.jpg');
    }

    /**
     * Create 'static' user model.
     *
     * @param string $login
     * @param string $password
     *
     * @return void
     */
    protected function createStaticUser(string $login, string $password,  string $email,  string $role = 'user', string $status, string $firstname, string $lastname, string $avatar ): void
    {

        if (User::where('login', '=', $login)->doesntExist()) {
            $this->log("Create static user model -> Login: <comment>{$login}</comment> | Password: <comment>{$password}</comment>  | Email: <comment>{$email}</comment> | Role: <comment>{$role}</comment>");

            factory(User::class)->create([
                'login'          => $login,
                'email'          => $email,
                'password'       => bcrypt($password),
                'role'           => $role,
                'deleted_at'     => null,
                'photo_original' => $avatar,
                'firstname'      => $firstname,
                'lastname'       => $lastname,
                'status'         => $status,
            ]);
        } else {
            $this->log("Static user model ({$login}:{$password}) already exists");
        }
    }

    /*
     * Batch models creating.
     *
     * @return void
     */

    protected function seedModelsBatch(): void
    {
        $rules = [
            User::class => random_int(50, 70),
        ];

        foreach ($rules as $class_name => $models_count) {
            $this->log(sprintf(
                '%s <comment>(%d %s)</comment>', $class_name, $models_count, Str::plural('model', $models_count)
            ));

            factory($class_name)->times($models_count)->create();
        }
    }

    /**
     * Log some message into console.
     *
     * @param string $message
     *
     * @return void
     */
    protected function log(string $message): void
    {
        $this->command->getOutput()->writeln("<comment>Seed:</comment> $message");
    }
}
