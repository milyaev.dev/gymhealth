<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('login')->nullable()->unique();
            $table->string('email')->nullable()->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('role',16)->default('user');
            $table->timestamps();
            $table->string('photo_small')->nullable();
            $table->string('photo_original')->nullable();
            $table->string('status', 16);
            $table->integer('facebook_id')->nullable()->unique();
            $table->integer('instagram_id')->nullable()->unique();
            $table->integer('google_id')->nullable()->unique();
            $table->integer('twitter_id')->nullable()->unique();
            $table->integer('telegtam_id')->nullable()->unique();
            $table->integer('vk_id')->nullable()->unique();
            $table->string('verify_token')->nullable()->unique();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
}
