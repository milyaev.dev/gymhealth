<?php

declare(strict_types = 1);

namespace App\Models\User;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use App\Models\User\AbstractBaseModel;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Str;

/**
 * App\Models\User
 *
 * @property int         $id
 * @property string      $login
 * @property string      $email
 * @property string      $password Password hash
 * @property string      $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string $role
 * @property string $status
 * @property string|null $photo_small
 * @property string|null $photo_original
 * @property int|null $facebook_id
 * @property int|null $instagram_id
 * @property int|null $google_id
 * @property int|null $twitter_id
 * @property int|null $telegtam_id
 * @property int|null $vk_id
 * @property string|null $verify_token
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAuthKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereInstagramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePasswordHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhotoOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhotoSmall($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTelegtamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTwitterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereVkId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereVerifyToken($value)
 */
class User extends AbstractBaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, Notifiable, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public const STATUS_WAIT = 'wait';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_BANNED= 'banned';

    public const ROLE_USER = 'user';
    public const ROLE_TRAINER = 'trainer';
    public const ROLE_MODER = 'moder';
    public const ROLE_ADMIN = 'admin';

    public const NO_PHOTO = NULL;


    protected $fillable = [
        'login',
        'email',
        'password',
        'remember_token',
        'firstname',
        'lastname',
        'photo_small',
        'photo_original',
        'role',
        'status',
        'facebook_id',
        'instagram_id',
        'google_id',
        'twitter_id',
        'telegram_id',
        'vk_id',
        'verify_token',
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    public static function rolesList(): array
    {
        return [
            self::ROLE_USER       => 'Пользователь',
            self::ROLE_TRAINER    => 'Тренер',
            self::ROLE_MODER      => 'Модератор',
            self::ROLE_ADMIN      => 'Администратор',
        ];
    }

    public static function statusList(): array
    {
        return [
            self::STATUS_WAIT     => 'В ожидание',
            self::STATUS_ACTIVE   => 'Активный',
            self::STATUS_BANNED   => 'Заблокированный',
        ];
    }

    public static function register(string $login, string $email, string $password): self
    {
        return static::create([
            'login' => $login,
            'email' => $email,
            'password' => bcrypt($password),
            'verify_token' => Str::uuid(),
            'role' => self::ROLE_USER,
            'status' => self::STATUS_WAIT,
        ]);
    }

    public static function new(string $login, string $email, string $password): self
    {
        return static::create([
            'login' => $login,
            'email' => $email,
            'password' => User::hashPassword($password),
            'role' => self::ROLE_USER,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public function isWait(): bool
    {
        return $this->status === self::STATUS_WAIT;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isBanned(): bool
    {
        return $this->status === self::STATUS_BANNED;
    }

    public function verify()
    {
        if (!$this->isWait()) {
            throw new \DomainException('User is already verified.');
        }

        $this->update([
            'status' => self::STATUS_ACTIVE,
            'verify_token' => null,
        ]);
    }

    public function changeRole($role): void
    {
        if (!array_key_exists($role, self::rolesList())) {
            throw new \InvalidArgumentException('Undefined role "' . $role . '"');
        }
        if ($this->role === $role) {
            throw new \DomainException('Role is already assigned.');
        }
        $this->update(['role' => $role]);
    }

    public function changeStatus($status): void
    {
        if (!array_key_exists($status, self::statusList())) {
            throw new \InvalidArgumentException('Undefined status "' . $status . '"');
        }
        if ($this->status === $status) {
            throw new \DomainException('Status is already assigned.');
        }
        $this->update(['status' => $status]);
    }

    public function isUser(): bool
    {
        return $this->role === self::ROLE_USER;
    }

    public function isTrainer(): bool
    {
        return $this->role === self::ROLE_TRAINER;
    }

    public function isModer(): bool
    {
        return $this->role === self::ROLE_MODER;
    }

    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function noPhoto(): bool
    {
        return $this->photo_original === self::NO_PHOTO;
    }

    public function setPhoto($photo_original): void
    {
        $this->update(['photo_original' => $photo_original]);
    }

    public function setRoleTrainer(): void
    {
        $this->update(['role' => User::ROLE_TRAINER]);
    }

    public function setRoleUser(): void
    {
        $this->update(['role' => User::ROLE_USER]);
    }

    public function setStatusBanned(): void
    {
        $this->update(['status' => User::STATUS_BANNED]);
    }

    public function setStatusWait(): void
    {
        $this->update(['status' => User::STATUS_WAIT]);
    }

    public static function hashPassword(string $password): string
    {
        return Hash::make($password);
    }

    protected function setPasswordAttribute($value): void
    {
        $value = (string) $value;

        // Hash input value only if needed
        if ((Hash::info($value)['algoName'] ?? 'unknown') !== config('hashing.driver')) {
            $value = static::hashPassword($value);
        }

        $this->attributes['password'] = $value;
    }

}
