<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User\User;

class HomeController extends Controller
{
    public const GOAL_USERS = 100;

    public function index()
    {
        $count_users = User::get()->count();

        //$count_users = User::all()->count();
        $active_users = User::where('status', 'active')->count();

        $goal_users = self::GOAL_USERS;
        $procent_users = $active_users / self::GOAL_USERS * 100;
        $need_users = self::GOAL_USERS - $active_users;

        $array = sys_getloadavg();
        $sysload = number_format(($array[0] + $array[1] + $array[2]) / 3,4);

        return view('admin.home', compact('count_users', 'active_users', 'procent_users', 'need_users', 'goal_users', 'sysload'));
    }
}
