<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\Users\CreateRequest;
use App\Http\Requests\Admin\Users\UpdateRequest;
use App\UseCases\Auth\RegisterService;
use App\Http\Controllers\Controller;
use App\Models\User\User;
use App\Http\Requests\Admin\UsersCreateRequest;

class UsersController extends Controller
{
    private $register;

    public function __construct(RegisterService $register)
    {
        $this->register = $register;
    }

    public function index(Request $request)
    {
        $query = User::orderByDesc('id');

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('login'))) {
            $query->where('login', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        if (!empty($value = $request->get('role'))) {
            $query->where('role', $value);
        }

        $users = $query->paginate(20);

        $statuses = User::statusList();

        $roles = User::rolesList();

        return view('admin.users.index', compact('users', 'statuses', 'roles'));
    }


    public function create()
    {
        return view('admin.users.create');
    }


    public function store(CreateRequest $request)
    {
        $user = User::new(
            $request['name'],
            $request['email'],
            $request['password']
        );

        return redirect()->route('admin.users.show', $user);
    }


    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }


    public function edit(User $user)
    {
        $roles = User::rolesList();
        $status = User::statusList();

        return view('admin.users.edit', compact('user', 'roles', 'status'));
    }


    public function update(UpdateRequest $request, User $user)
    {
        $user->update($request->only(['login', 'email', 'firstname', 'lastname']));

        if ($request['role'] !== $user->role) {
            $user->changeRole($request['role']);
        }

        if ($request['status'] !== $user->status) {
            $user->changeStatus($request['status']);
        }

        return redirect()->route('admin.users.show', $user);
    }

    public function all(Request $request)
    {
        $query = User::orderByDesc('id');

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('login'))) {
            $query->where('login', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        if (!empty($value = $request->get('role'))) {
            $query->where('role', $value);
        }

        $users = $query->paginate(20);

        $statuses = User::statusList();

        $roles = User::rolesList();

        return view('admin.users.index', compact('users', 'statuses', 'roles'));
    }


    public function deletephoto(Request $request, User $user)
    {
        $photo_orignal = '';
        $user->setPhoto($request[$photo_orignal]);
        return redirect()->route('admin.users.show', $user);
    }


    public function destroy($id)
    {

    }

    public function verify(User $user)
    {
        $this->register->verify($user->id);

        return redirect()->route('admin.users.show', $user);
    }

    public function setTrainer(User $user)
    {
        $user->setRoleTrainer();
        return redirect()->route('admin.users.show', $user);
    }

    public function setUser(User $user)
    {
        $user->setRoleUser();
        return redirect()->route('admin.users.show', $user);
    }

    public function setBanned(User $user)
    {
        $user->setStatusBanned();
        return redirect()->route('admin.users.show', $user);
    }

    public function setWait(User $user)
    {
        $user->setStatusWait();
        return redirect()->route('admin.users.show', $user);
    }
}
