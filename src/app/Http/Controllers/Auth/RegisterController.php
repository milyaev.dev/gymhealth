<?php
/**
 * Created by PhpStorm.
 * User: headb
 * Date: 13.04.2019
 * Time: 15:38
 */

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\UseCases\Auth\RegisterService;
use App\Mail\Auth\VerifyMail;
use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
//use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    private $service;

    public function __construct(RegisterService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    public function showRegisterForm()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $user = User::register(
            $request['login'],
            $request['email'],
            $request['password']
        );

        Mail::to($user->email)->send(new VerifyMail($user));
        //Mail::to($user->email)->queue(new VerifyMail($user)); очередь
        event(new Registered($user));

        return redirect()->route('login')
            ->with('success', 'Check your email anc click on the link to verify');
    }

    public function verify($token)
    {
        if(!$user = User::where('verify_token', $token)->first()) {
            return redirect()->route('login')
                ->with('error', 'Sorry your link cannot be identified.');
        }

        try {
            $this->service->verify($user->id);
            return redirect()->route('login')->with('success', 'Your e-mail is verified. You can now login.');
        } catch (\DomainException $e) {
            return redirect()->route('login')->with('error', $e->getMessage());
        }
    }
}
