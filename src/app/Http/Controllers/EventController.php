<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User\User;

class EventController extends AbstractController
{
    public function index()
    {
        return view('event.index', [
            'users' => User::all(),
        ]);
    }

    public function workout()
    {
        //return view('event.workout.index');
        return view('event.workout.index');
    }

    public function meal()
    {
        return view('event.meal.index');
    }

    public function sleep()
    {
        return view('event.sleep.index');
    }

}
