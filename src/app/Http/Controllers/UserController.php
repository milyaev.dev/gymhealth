<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends AbstractController
{
    public function profile()
    {
        return view('user.profile');
    }

    public function editProfile()
    {
        return view('user.edit');
    }

    public function logout()
    {

    }
}
