<?php

namespace App\Http\Requests\Admin\Users;

use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property User $user
 */
class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        //dd($this->user->id);die;
        return [
            'login'     => 'required|string|max:255|unique:users,login,'.$this->user->id,
            'email'     => 'required|email|max:255|unique:users,email,'.$this->user->id,
            'firstname' => 'required|string',
            'lastname'  => 'required|string',
        ];
    }
}
