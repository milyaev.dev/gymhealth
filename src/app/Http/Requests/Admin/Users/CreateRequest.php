<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'login'     => 'required|string|max:255|unique:users,login',
            'email'     => 'required|email|max:255|unique:users,email',
            'password'  => 'required|string',
        ];
    }
}
