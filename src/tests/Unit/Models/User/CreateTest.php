<?php

namespace Tests\Unit\Models\User;

use App\Models\User\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\AbstractTestCase;

class CreateTest extends AbstractTestCase
{
    use DatabaseTransactions;

    public function testNew(): void
    {
        $user = User::new(
            $login = 'login',
            $email = 'email@mail.com',
            $password = 'password'
        );

        self::assertNotEmpty($user);

        self::assertEquals($login, $user->login);
        self::assertEquals($email, $user->email);
        self::assertNotEmpty($user->password);
        self::assertNotEquals($password, $user->password);

        self::assertFalse($user->isWait());
        self::assertTrue($user->isActive());
    }
}
