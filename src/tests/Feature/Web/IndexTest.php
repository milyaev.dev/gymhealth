<?php

namespace Tests\Feature\Web;

use AvtoDev\DevTools\Tests\PHPUnit\AbstractLaravelTestCase;

class IndexTest extends AbstractLaravelTestCase
{
    /**
     * Test index page opening.
     *
     * @return void
     */
    public function testIndexPage(): void
    {
        $this->get('/')->assertSee('GymHealth')->assertSuccessful();
    }
}
