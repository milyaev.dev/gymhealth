<?php

namespace Tests\Feature;

use App\Models\User\User;
use Illuminate\Support\Str;
use AvtoDev\DevTools\Tests\PHPUnit\AbstractLaravelTestCase;

abstract class AbstractFeatureTestCase extends AbstractLaravelTestCase
{
    /**
     * @var string
     */
    protected $admin_user_login;

    /**
     * @var string
     */
    protected $admin_user_email;

    /**
     * @var string
     */
    protected $admin_user_auth_key;

    /**
     * @var string
     */
    protected $admin_user_password;

    /**
     * @var User
     */
    protected $admin_user;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->admin_user_login    = 'feature_test_' . $this->faker->userName;
        $this->admin_user_email    = 'feature_test_' . $this->faker->email;
        $this->admin_user_password = Str::upper(Str::random(6)) . Str::random(6) . random_int(1, 50) . '!';

        $this->admin_user = User::create([
            'login'             => $this->admin_user_login,
            'email'             => $this->admin_user_email,
            'password'          => $this->admin_user_password,
            'role'              => User::ROLE_USER,
            'status'            => User::STATUS_ACTIVE,
            'verify_token'      => Str::uuid(),
        ]);

        $this->startSession();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->flushSession();

        $this->admin_user->forceDelete();

        parent::tearDown();
    }
}
